steps = [
    [
        ## Create the table
        """
        CREATE TABLE accounts (
            id SERIAL PRIMARY KEY NOT NULL,
            username VARCHAR(100) NOT NULL,
            full_name VARCHAR(100) NOT NULL,
            hashed_password VARCHAR(100) NOT NULL
        );
        """,
        ## Drop the table
        """
        DROP TABLE accounts;
        """
    ]
]
