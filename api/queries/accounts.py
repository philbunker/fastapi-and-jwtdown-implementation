from pydantic import BaseModel
from queries.pool import pool
from models import AccountIn, AccountOut, AccountOutWithPassword


class AccountRepository:
    def get(self, username: str) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        '''
                        SELECT id, username, full_name, hashed_password
                        FROM accounts
                        WHERE username = %s
                        ''',
                        [
                            username
                        ]
                    )
                    account = [
                        AccountOutWithPassword(
                            id=record[0],
                            username=record[1],
                            full_name=record[2],
                            hashed_password=record[3]
                        ) for record in db][0]
                    print('queries.accounts: account: ', account)
                    return account
        except Exception as e:
            print(e)
            return {"message": "error"}

    def create(self, info: AccountIn, hashed_password: str) -> AccountOutWithPassword:
        print(info)
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO accounts
                            (username, full_name, hashed_password)
                        VALUES
                            (%s, %s, %s)
                        RETURNING id, username, full_name, hashed_password
                        """,
                        [
                            info.username,
                            info.full_name,
                            hashed_password
                        ]
                    )
                    id, username, full_name, hashed_password = result.fetchall()[0]

                    account_with_password = AccountOutWithPassword(id=id, username=username, full_name=full_name, hashed_password=hashed_password)

                    print(account_with_password)
                    return account_with_password
        except Exception as e:
            print(e)
            return {"message": "failed to create object"}

    # def vacation_in_to_out(self, id: int, vacation: AccountIn) -> AccountOutWithPassword:
    #     old_data = vacation.dict()
    #     return AccountOut(id=id, **old_data)
