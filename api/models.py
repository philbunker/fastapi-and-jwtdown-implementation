from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token


class DuplicateAccountError(BaseModel):
    pass

class AccountIn(BaseModel):
    username: str
    full_name: str
    password: str

class AccountOut(BaseModel):
    id: int
    username: str
    full_name: str

class AccountOutWithPassword(BaseModel):
    id: str
    username: str
    full_name: str
    hashed_password: str

class AccountToken(Token):
    account: AccountOut

class AccountForm(BaseModel):
    username: str
    password: str
